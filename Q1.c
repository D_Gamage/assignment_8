/*Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far. First name of the student, subject and its
marks for each student needs to be recorded. Data should be fetched through user input (do not hard code) and print the data back. Program should record and display
information of minimum 5 students.
Hint: use array to hold structures and use loops to access structures in the array*/
#include<stdio.h>
#define MAX 100
struct student{
    char first_name[20];
    char subject[20];
    float marks;
};
int main(){
   struct student stu[MAX];

   int i,x;
   printf("Enter Number of Students--->");
   scanf("%d",&x);
   for (i=0;i<x;i++)
   {
       printf("Enter Student's first Name---->");
       scanf("%s",stu[i].first_name);
       printf("Enter subject---->");
       scanf("%s",stu[i].subject);
       printf("Enter marks---->");
       scanf("%f",&stu[i].marks);
   }
   printf("\n***Student's Report***\n");
      for (i=0;i<x;i++)
   {
       printf("%d.First name of the student=%s\nSubject=%s \tMarks=%0.2f\n",i+1,stu[i].first_name,stu[i].subject,stu[i].marks);
       printf("\n");
   }
   return 0;

}


